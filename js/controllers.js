angular.module('visualization.controllers', [])
  .controller('AppCtrl', function($scope, socket) {
    $scope.connectedbeacons = {};
    count = 0;

    socket.on('test', function (data) {
      console.log("test!! was received with: "+data);
    });

    socket.on('test:test', function (data) {
      console.log("test was received with: "+data);
    });

    socket.on('beacon:detect', function(data){
      var updateBeacons = {},
          newBeacons = {};

      for (var item in data.beacons){ // go through all detected beacons
        var uniqueID = data.id+":"+item;

        if (uniqueID in $scope.connectedbeacons){ //check if the detected beacon is already in the list
          updateBeacons[uniqueID] = data.beacons[item];
          updateBeacons[uniqueID]["user"] = data.id;
        } else { //new detected beacon
          newBeacons[uniqueID] = data.beacons[item];
          newBeacons[uniqueID]["user"] = data.id;
        }

        $scope.connectedbeacons[uniqueID] = data.beacons[item];
        $scope.connectedbeacons[uniqueID]["user"] = data.id;

      }
      //update existing beacons
      for (var j in $scope.connectedbeacons){
        if (!(j in updateBeacons) && !(j in newBeacons)){ //check if one beacon is in the scope that is not detected anymore
          delete $scope.connectedbeacons[j];
          console.log("removed: "+j);
        }
      }
    });

    socket.on('beacon:enter', function(data){
      console.log(data.id+" has entered beacon: "+data.beacon.major);
    });

    socket.on('beacon:left', function(data){
      //console.log(data.id+" has left beacon: "+data.beacon.major);
    });
  })

  .controller('MapCtrl', function($scope, socket){
    console.log('MapCtrl');
  })
;

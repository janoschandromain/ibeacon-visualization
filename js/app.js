angular.module('visualization', ['ui.router', 'ui.bootstrap', 'visualization.controllers', 'visualization.services', 'btford.socket-io'])

  .run(function() {

  })

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('list', {
        url: '/list',
        templateUrl: 'templates/eventlist.html',
        controller: 'AppCtrl'
      })
      .state('map', {
        url: '/map',
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      })
    ;
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/list');
  });
